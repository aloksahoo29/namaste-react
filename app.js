// const element = React.createElement('h1', {id: 'heading', xyz:'abc'}, "Hello world from React");
// 
// root.render(element);

const root = ReactDOM.createRoot(document.getElementById('root'));
const parent = React.createElement('div', {id:'parent'}, 
    [React.createElement('div', {id:'child1'},
    [React.createElement('h1', {}, "i am h1 tag"), React.createElement('h2', {}, "i am h2 tag")]),

    React.createElement('div', {id:'child2'},
    [React.createElement('h1', {}, "i am h1 tag"), React.createElement('h2', {}, "i am h2 tag")])]
);

console.log(parent);

root.render(parent);